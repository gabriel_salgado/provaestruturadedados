
public class Questao4 implements IProva {

	public String[] ordenar(String[] dadosentrada, boolean crescente) {
		if (crescente) {
			QuickSortCrescente quicksort = new QuickSortCrescente();
			quicksort.Ordenar(0, dadosentrada.length - 1, dadosentrada);
		} else {
			QuickSortDecrescente quicksort = new QuickSortDecrescente();
			quicksort.Ordenar(0, dadosentrada.length - 1, dadosentrada);
		}
		return dadosentrada;
	}
}
