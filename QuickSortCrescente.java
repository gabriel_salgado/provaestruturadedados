public class QuickSortCrescente {

	public void Ordenar(int inicio, int fim, String vetor[]) {
		int pivo;
		if (inicio < fim) {
			pivo = Dividir(inicio, fim, vetor);
			Ordenar(inicio, pivo - 1, vetor);
			Ordenar(pivo + 1, fim, vetor);
		}
	}

	public int Dividir(int inicio, int fim, String[] vetor) {
		int esquerda, direita;
		String pivo, aux;
		esquerda = inicio;
		direita = fim;
		pivo = vetor[inicio];
		while (esquerda < direita) {
			while (vetor[esquerda].charAt(0) <= pivo.charAt(0) && esquerda < fim)
				esquerda++;
			while (vetor[direita].charAt(0) > pivo.charAt(0) && direita > inicio)
				direita--;
			if (esquerda < direita) {
				aux = vetor[esquerda];
				vetor[esquerda] = vetor[direita];
				vetor[direita] = aux;
			}

		}
		vetor[inicio] = vetor[direita];
		vetor[direita] = pivo;
		return direita;
	}

}
