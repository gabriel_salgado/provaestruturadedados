public class Fila {

	private int inicio;
	private int fim;
	private int vetor[];

	public Fila(int tamanho) {
		vetor = new int[tamanho];
		inicio = 0;
		fim = 0;
	}

	public void Queue(int inteiro) {
		if (fim != vetor.length) {
			vetor[fim] = inteiro;
			fim++;
		} else if (fim == vetor.length && inicio != 0) {
			Arrumar();
			vetor[fim] = inteiro;
			fim++;
		} else
			throw new RuntimeException("Pilha Cheia");
	}

	public int Dequeue() {
		if (fim == 0) {
			throw new RuntimeException("Pilha Vazia");
		} else {
			int aux = vetor[inicio];
			vetor[inicio] = 0;
			inicio++;
			return aux;
		}
	}

	private void Arrumar() {
		int j = 0;
		for (int i = 1; i < vetor.length; i++) {
			vetor[j] = vetor[i];
			j++;
		}
		fim -= 1;
		inicio--;

	}

	protected int Primeiro() {
		if (fim == 0) {
			throw new ArrayIndexOutOfBoundsException("Pilha Vazia");
		} else
			return vetor[(fim - 1)];
	}

}
