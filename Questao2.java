import java.util.Scanner;

public class Questao2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean igual = true;
		System.out.print("Informe a frase/palavra para saber se ela � pal�ndroma: ");
		String palavra = input.next();
		Pilha pilha = new Pilha(palavra.length());
		Pilha pilha2 = new Pilha(palavra.length());

		for (int i = 0; i < palavra.length(); i++) {
			pilha.Push(palavra.charAt(i));
		}
		for (int i = palavra.length(); i != 0; i--) {
			pilha2.Push(palavra.charAt(i-1));
		}
		for (int i = 0; i < palavra.length(); i++) {
			if (pilha.Pop() != pilha2.Pop()) {
				igual = false;
			}
		}
		if (igual)
			System.out.println("A palavra " + palavra + " � pal�ndroma");
		else
			System.out.println("A palavra " + palavra + " n�o � pal�ndroma");
	}

}
