import java.util.Scanner;

public class Questao1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean igual = true;
		System.out.print("Indique o tamanho da primeira fila: ");
		int tamanho1 = input.nextInt();
		System.out.print("Indique o tamanho da segunda fila: ");
		int tamanho2 = input.nextInt();
		Fila fila1 = new Fila(tamanho1);
		Fila fila2 = new Fila(tamanho2);
		Fila fila1aux = new Fila(tamanho1);
		Fila fila2aux = new Fila(tamanho2);
		for (int i = 0; i < tamanho1; i++) {
			System.out.println("Insira o " + (i + 1) + " n�mero da primeira fila");
			int num = input.nextInt();
			fila1.Queue(num);
		}
		for (int i = 0; i < tamanho2; i++) {
			System.out.println("Insira o " + (i + 1) + " n�mero da segunda fila");
			int num = input.nextInt();
			fila2.Queue(num);
		}
		if (tamanho1 != tamanho2) {
			igual = false;
		} else {
			for (int i = 0; i < tamanho1; i++) {
				int aux1 = fila1.Dequeue();
				int aux2 = fila2.Dequeue();
				fila1aux.Queue(aux1);
				fila2aux.Queue(aux2);
				if (aux1 != aux2) {
					igual = false;
				}
			}
			for (int i = 0; i < tamanho1; i++) {
				fila1.Queue(fila1aux.Dequeue());
				fila2.Queue(fila2aux.Dequeue());
			}
		}
		if (igual)
			System.out.println("As filas s�o iguais");
		else
			System.out.println("As filas n�o s�o iguais");
	}

}
