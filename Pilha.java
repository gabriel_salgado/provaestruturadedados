
public class Pilha {

	private char vetor[];
	private int qtdpilha = 0;

	public Pilha(int tamanho) {
		vetor = new char[tamanho];
	}

	public void Push(char elemento) {
		if (qtdpilha > vetor.length)
			throw new ArrayIndexOutOfBoundsException("Pilha Cheia");
		else {
			vetor[qtdpilha] = elemento;
			qtdpilha++;
		}
	}

	public char Pop() {
		if (qtdpilha <= 0)
			throw new ArrayIndexOutOfBoundsException("Pilha Vazia");
		else {
			char aux;
			aux = vetor[qtdpilha-1];
			vetor[qtdpilha-1] = 0;
			qtdpilha--;
			return aux;
		}
	}

	public char Top() {
		if (qtdpilha <= 0)
			throw new ArrayIndexOutOfBoundsException("Pilha Vazia");
		else
			return vetor[qtdpilha - 1];
	}
}
